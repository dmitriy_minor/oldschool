#include <vector>
#include <algorithm>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <map>
#include <mutex>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cmath>
#include "defines.h"
#include "rtpstat.hpp"

/* stub to add extra debugging/logging... */
static void debugprint(const char *Format, ...)
{
}

struct rtp_header_r
{
#if __BYTE_ORDER == __BIG_ENDIAN
        unsigned char version:2;       // Version, currently 2
        unsigned char padding:1;       // Padding bit
        unsigned char extension:1;     // Extension bit
        unsigned char cc:4;            // CSRC count
        unsigned char marker:1;        // Marker bit
        unsigned char payload:7;       // Payload type
#else
        unsigned char cc:4;            // CSRC count
        unsigned char extension:1;     // Extension bit
        unsigned char padding:1;       // Padding bit
        unsigned char version:2;       // Version, currently 2
        unsigned char payload:7;       // Payload type
        unsigned char marker:1;        // Marker bit
#endif
    uint16_t seq:16;
    uint32_t ts;
    uint32_t ssrc;
};

/* RTP stat implementation */

RtpStat::RtpStat()
{
  active = false;
  media_started = false;
  max_media_interval = 0;
  com_media_interval = 0;
  silent_start = NULL;
  media_start = NULL;
  rtp_empty_byte = 0x00;
  noise_protection = 0;
}

unsigned int RtpStat::GetMilliseconds(timespec *ts)
{
  // Convert ts to milliseconds
  return (int)(ts[0].tv_sec * 1000 + ts[0].tv_nsec/1000000);
}

unsigned int RtpStat::GetTimeDifference(timespec *ts1, timespec *ts2)
{
  return (int)(GetMilliseconds(ts1) - GetMilliseconds(ts2));
}

unsigned char RtpStat::GetEmptyByte(const char ptype)
{
  switch (ptype){
    case 8:
      return(0xd5);
    case 0:
      return(0xff);
    default:
      return(0x00);
  }
}

void RtpStat::PrintPacketHex(const char *msg, const unsigned char *buf, uint nbytes)
{
  char line[128], *p;
  int len = sizeof(line);
  int nchar;
  uint i;

  if (msg && (msg[0] != '\0'))
    debugprint("%s:", msg);

  p = line;
  for (i = 0; i < nbytes; i++) {
    if (i % 16 == 0) {
      nchar = snprintf(p, len, "  0x%04x: ", i);  /* line prefix */
      p += nchar;
      len -= nchar;
    }
    if (len > 0) {
      nchar = snprintf(p, len, "%02x ", (unsigned char)buf[i]);
      p += nchar;
      len -= nchar;
    }
    if (i % 16 == 15) {
      debugprint("%s", line);    /* flush line */
      p = line;
      len = sizeof(line);
    }
  }
  /* flush last partial line */
  if (p != line)
    debugprint("%s", line);
}

bool RtpStat::IsPacketEmpty(const unsigned char* buf, uint nbytes)
{
  if (rtp_empty_byte == 0x00)
  {
    // payload type not supported.
    return false;
  }
  unsigned int empty_byte_c = 0;
  for (uint i = header_size; i<nbytes; i++)
  {
    if(empty_byte_c >= 80)
    {
      // Packet is empty;
      debugprint("Empty packet detected. SSRC: 0x%04x, RT: %s:%d", ssrc_id, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
      //PrintPacketHex(NULL, buf, nbytes);
      return true;
    }
    if(buf[i] == rtp_empty_byte)
    {
      empty_byte_c++;
    }
    else
    {
      empty_byte_c = 0;
    }
  }
  // Packet not empty
  return false;
}

uint RtpStat::GetMaxMediaInterval()
{
  timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  if(media_start)
  {
    uint time_diff;
    if((time_diff = GetTimeDifference(&now, &media_start_t)) > max_media_interval)
    {
      return time_diff;
    }
    else
    {
      return max_media_interval;
    }
  }
  else
  {
    return max_media_interval;
  }
}

uint RtpStat::GetComMediaInterval()
{
  timespec now; 
  clock_gettime(CLOCK_REALTIME, &now);
  if(media_start)
  {
    com_media_interval += GetTimeDifference(&now, &media_start_t);
  }
  return com_media_interval;
}

void RtpStat::MediaStart(timespec *cur_ts)
{
  if(!media_start)
  {
    media_start_t = *cur_ts;
    media_start = &media_start_t;
  }
}

void RtpStat::SilentStart(timespec *cur_ts)
{
  if(media_start)
  {
    uint time_diff;
    if((time_diff = GetTimeDifference(cur_ts, media_start)) > max_media_interval)
    {
      max_media_interval = time_diff;
    }
    com_media_interval += time_diff;
    media_start = NULL;
  }
  if(!silent_start)
  {
    silent_start_t = *cur_ts;
    silent_start = &silent_start_t;
  }
}

void RtpStat::SilentStop(timespec *cur_ts)
{
  if(!media_start)
    MediaStart(cur_ts);

  debugprint("Silent interval detected. Interval: %dms. SSRC: 0x%04x, RT: %s:%d", GetTimeDifference(cur_ts, silent_start),
          ssrc_id, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
  int time_diff;
  if((time_diff = GetTimeDifference(cur_ts, silent_start)) > max_packet_delta)
  {
    //Update max packet delta;
    max_packet_delta = time_diff;
  }
  silent_start = NULL;
}


int RtpStat::GetSilentInterval()
{
  std::vector<int> results;
  int time_diff = GetTimeDifference(&stop_t, &last_recv_t);
  if(silent_start)
  {
    // Drop silent interval if it less than RTP_NOISE_COUNTER * require_packet_diff.
    int silent_int = GetTimeDifference(&stop_t, silent_start);
    results.push_back(silent_int >= (RTP_NOISE_COUNTER * require_packet_diff) ? silent_int : 0);
  }
  results.push_back(media_started != true ? GetTimeDifference(&stop_t, &start_t) : 0);
  results.push_back(std::abs(max_packet_delta - require_packet_diff) > max_diff_error ? max_packet_delta: 0);
  results.push_back(time_diff > (require_packet_diff + max_diff_error) ? time_diff: 0);
  return *max_element(results.begin(), results.end());
}

void RtpStat::Initialize(unsigned const char *buf, int nbytes, timespec *cur_ts, sockaddr_in *from)
{
  require_packet_diff = REQ_PACKET_DIFF;
  max_diff_error = MAX_DIFF_ERROR;
  media_started = false;
  max_media_interval = 0;
  com_media_interval = 0;
  silent_start = NULL;
  media_start = NULL;
  max_packet_delta = 0;
  empty_packet_c = 0;
  noise_protection = 0;
  rtp_header_r * rtp_header = (rtp_header_r*)buf;
  last_recv_t = *cur_ts;
  remote_addr = *from;
  payload_type = rtp_header->payload;
  rtp_empty_byte = GetEmptyByte(payload_type);
  header_size = sizeof(rtp_header_r);
  ssrc_id = ntohl(rtp_header->ssrc);
  last_seq = ntohs(rtp_header->seq);
}

bool RtpStat::HandlePacket(unsigned const char *buf, int nbytes, timespec *cur_ts, sockaddr_in *from)
{
  if(!active)
  {
    //Measure not started, exit.
    return false;
  }
  // Is first packet?
  if(packet_c == 0)
  {
    Initialize(buf, nbytes, cur_ts, from);
    // Increment packet counter
    packet_c++;
    return true;
  }

  rtp_header_r * rtp_header = (rtp_header_r*)buf;
  remote_addr = *from;

  // Increment packet counter
  packet_c++;

  /* Check ssrc_id */
  if (ssrc_id != ntohl(rtp_header->ssrc))
  {
    ssrc_id = ntohl(rtp_header->ssrc);
  } // check ssrc complete

  /* Check sequense number */
  uint16_t exp_seq = last_seq + 1;
  uint16_t cur_seq = ntohs(rtp_header->seq);
  if(exp_seq != cur_seq)
  {
      debugprint("Wrong sequence number. Expected seq: %d, received seq: %d. SSRC: 0x%04x, RT: %s:%d", exp_seq, cur_seq, 
              ssrc_id, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
  }
  last_seq = cur_seq; // check sequence complete

  // Update payload type if needed.
  if (rtp_header->payload != payload_type)
  {
    payload_type = rtp_header->payload;
    rtp_empty_byte = GetEmptyByte(payload_type);
  } // update ptype complete

  // Receive comfort noise packet, start silient mesuring.
  if(payload_type == CN_PT)
  {
    comfort_noise = true;
    debugprint("CN received. SSRC: 0x%04x, RT: %s:%d", ssrc_id, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
    SilentStart(cur_ts);
    return true;
  }

  if (!comfort_noise)
  {
    int cur_diff = GetTimeDifference(cur_ts, &last_recv_t);
    if(std::abs(cur_diff - require_packet_diff) > max_diff_error)
    {
      SilentStart(&last_recv_t);
      debugprint("Wrong time difference between packets: %d. SSRC: 0x%04x, RT: %s:%d", cur_diff, ssrc_id,
              inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
    }
  } // Check time difference between packet has been completed. 
  last_recv_t = *cur_ts;
  // Check if packet empty
  if(IsPacketEmpty(buf, nbytes))
  {
    empty_packet_c++;
    // Handle empty packet only if media is started
    if(media_started)
    {
      // Received empty packet when media started.
      // Start measuring of silence
      SilentStart(cur_ts);
      return true;
    }
    else
    {
      noise_protection = 0;
    }
  }
  else
  {
    // check noise_protection counter
    if(noise_protection == 0)
    {
      first_media_packet = *cur_ts;
    }
    if(noise_protection < RTP_NOISE_COUNTER)
    {
      noise_protection++;
      return true;
    }
    if(!media_started)
    {
      debugprint("Media stream started. SSRC: 0x%04x, RT: %s:%d", ssrc_id, inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port));
      MediaStart(&first_media_packet);
      media_started = true;
    }
    else
    {
      if(silent_start)
        SilentStop(cur_ts);
      if(comfort_noise)
        comfort_noise = false;
    }
  }
  return true;
}

bool RtpStat::StartSilentMeasure()
{
  clock_gettime(CLOCK_REALTIME, &start_t);
  //Drop packet counter
  packet_c = 0;
  active = true;
  return true;
}

bool RtpStat::StopSilentMeasure()
{
  if(!active)
  {
    debugprint("Can't stop silent measuring. Cause: silent measuring not started now.");
    return false;
  }
  clock_gettime(CLOCK_REALTIME, &stop_t);
  active = false;
  return true;
}

RtpStat::~RtpStat()
{
  debugprint("Remove stat object");
}

/* StatMap implementation */

void StatMap::AddTask(int key, void* value)
{
  StreamMux.lock();
  if(!TaskMap.insert(std::make_pair(key, value)).second)
  {
    WARNING("Can't add stream. Stream already exist");
  }
  StreamMux.unlock();
}

void StatMap::DelTask(int key)
{
  std::map <int, void*>::iterator rtp_stat;
  StreamMux.lock();
  if ((rtp_stat = TaskMap.find(key)) != TaskMap.end())
  {
    TaskMap.erase(key);
  }
  StreamMux.unlock();
}

void* StatMap::GetTask(int key)
{
  std::map <int, void*>::iterator task_it;
  void* result;
  StreamMux.lock();
  if ((task_it = TaskMap.find(key)) == TaskMap.end())
  {
    result = NULL;
  }
  else
  {
    result = task_it->second;
  }
  StreamMux.unlock();
  return result;
}

StatMap::StatMap(){}

StatMap::~StatMap()
{
  std::map<int, void*>::iterator it;
  StreamMux.lock();
  TaskMap.clear();
  StreamMux.unlock();
}