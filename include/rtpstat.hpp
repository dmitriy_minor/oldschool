#ifndef __RTPSTAT__
#define __RTPSTAT__

#define CN_PT 13
#define REQ_PACKET_DIFF 20
#define MAX_DIFF_ERROR 10
#define RTP_NOISE_COUNTER 5

struct rtp_header_r;


class RtpStat
{
public:
    RtpStat();
    bool HandlePacket(unsigned const char*, int nbytes, timespec*, sockaddr_in*);
    bool StartSilentMeasure();
    bool StopSilentMeasure();
    int GetSilentInterval();
    uint GetMaxMediaInterval();
    uint GetComMediaInterval();
    ~RtpStat();

private:
    void Initialize(unsigned const char *, int, timespec *, sockaddr_in *);
    void PrintPacketHex(const char *, const unsigned char *, uint nbytes);
    void SilentStart(timespec *cur_ts);
    void SilentStop(timespec *cur_ts);
    void MediaStart(timespec *cur_ts);
    bool IsPacketEmpty(const unsigned char*, uint);
    unsigned int GetTimeDifference(timespec*, timespec*);
    unsigned int GetMilliseconds(timespec*);
    unsigned char GetEmptyByte(const char);

private:
    uint16_t      last_seq;
    uint32_t      ssrc_id;
    sockaddr_in   remote_addr;
    bool          active;
    bool          media_started;
    bool          comfort_noise;
    int           require_packet_diff;
    int           max_diff_error;
    unsigned int  payload_type;
    unsigned int  noise_protection;
    unsigned int  header_size;
    unsigned char rtp_empty_byte;
    unsigned long empty_packet_c;
    unsigned long packet_c;
    unsigned int  max_media_interval;
    unsigned int  com_media_interval; 
    int           max_packet_delta;
    timespec      first_media_packet;
    timespec      last_recv_t;
    timespec      silent_start_t;
    timespec      *silent_start;
    timespec      start_t;
    timespec      stop_t;
    timespec      media_start_t;
    timespec      *media_start;
};

class StatMap
{
public:
    void AddTask(int, void*);
    void DelTask(int key);
    void* GetTask(int key);
    ~StatMap();
    StatMap();
private:
    std::map <int, void*> TaskMap;
    std::mutex StreamMux;
};

#endif